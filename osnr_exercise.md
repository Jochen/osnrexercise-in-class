---
theme: chalmers.css
---

<!-- .slide: data-background="./images/fiber_titlebg.png" -->

# OSNR exercise 

In todays exercise we will be using the 58 equation to design a submarine fiber link 


---


$ OSNR(dB) = P_{in}(dBm) - N(dB) - NF(dB)- G(dB) + 58dBm $

---

# Questions?

Before we start does anyone have a question?

---

# Exercise

For the exercise we will be using a Python notebook. This is a Python programming environment 
running in the browser. Please go to the following link:

https://deepnote.com/project/OSNR-learning-EEHoURgNSga2f3z-pLNmew/%2Fnotebook.ipynb

---

# Summary

So today we learned about how you would design a link based on the OSNR. To review the task please go to:

https://www.menti.com/i8stgxuyt9

